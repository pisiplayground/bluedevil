#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


def setup():
    shelltools.system("convert src/mime/codeblocks.png +set date:create +set date:modify -background none -extent 64x64 src/mime/codeblocks.png")
    options = ''.join([
              '-Wno-unused-result ',
              '-Wno-stringop-overflow ',
              '-Wno-deprecated-declarations'
    ])
    options_cfg = ''.join([
              '--enable-unicode ',
              '--with-contrib-plugins=all ',
              '--with-wx-config=/usr/bin/wx-config-gtk3'
              ])
    #shelltools.system("./bootstrap")
    #autotools.autoreconf("-vif")
    #plugins = "AutoVersioning,BrowseTracker,byogames,Cccc,CppCheck,cbkoders,codesnippets,codestat,copystrings,dragscroll,envvars,headerfixup,help,hexeditor,incsearch,keybinder,MouseSap,profiler,regex,exporter,symtab,Valgrind"
    # suppress compiler warnings
    pisitools.cflags.add("-Wno-unused-result")
    pisitools.cxxflags.add("%s" % options)
    
    autotools.configure("%s" % options_cfg)

    # disable rpath
    pisitools.dosed("libtool", "^hardcode_libdir_flag_spec=.*", "hardcode_libdir_flag_spec=\"\"")
    pisitools.dosed("libtool", "^runpath_var=LD_RUN_PATH", "runpath_var=DIE_RPATH_DIE")
    
    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")


def build():
    autotools.make()

def install():
    autotools.install()

    pisitools.dodoc("COPYING", "README")