# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import pythonmodules

shelltools.export("JOBS", get.makeJOBS().replace("-j", ""))

def build():
    pythonmodules.compile(pyVer="3")

def install():
    pythonmodules.install(pyVer="3")

    # rename binaries to avoid conflict with thw py2 version of wx package
    pisitools.rename("/usr/bin/img2py", "img2py-py3")
    pisitools.rename("/usr/bin/pywxrc", "pywxrc-py3")
    pisitools.rename("/usr/bin/img2xpm", "img2xpm-py3")
    pisitools.rename("/usr/bin/img2png", "img2png-py3")
    pisitools.rename("/usr/bin/pycrust", "pycrust-py3")
    pisitools.rename("/usr/bin/pyshell", "pyshell-py3")
    pisitools.rename("/usr/bin/helpviewer", "helpviewer-py3")

    pisitools.dodoc("LICENSE*")
