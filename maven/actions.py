#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

import os
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


def build():
    # add maven script -mvn- to path
    #shelltools.export("PATH", "%s:%s/%s-%s/bin" % (os.environ.get("PATH"), get.workDIR(), get.srcNAME(), get.srcVERSION()))
    shelltools.system("echo $PATH")
    shelltools.system("mvn --version")
    shelltools.system("mvn -Drat.skip=true package -Dmaven.repo.local='%s/apache-maven-3.6.3/repo' -Dproject.build.sourceEncoding=UTF-8 -e" % get.workDIR())


def install():
    # add maven script -mvn- to path
    #shelltools.export("PATH", "%s:%s/%s-%s/bin" % (os.environ.get("PATH"), get.workDIR(), get.srcNAME(), get.srcVERSION()))
    shelltools.system("mvn -Drat.skip=true install \
                      -Dmaven.repo.local='%s/apache-maven-3.6.3/repo' \
                      -Dproject.build.sourceEncoding=UTF-8 -e \
                      -DdistributionTargetDir='%s/opt/maven' \
                      -DskipTests -Dmaven.test.skip=true" % (get.workDIR(), get.installDIR()))

    pisitools.dodir("/etc/profile.d")
    pisitools.dosym("/opt/maven/bin/mvn", "/usr/bin/mvn")
    pisitools.dosym("/opt/maven/bin/mvnDebug", "/usr/bin/mvnDebug")
    
    pisitools.dodoc("LICENSE", "README*")
