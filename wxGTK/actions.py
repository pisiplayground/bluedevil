# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

def setup():
    options = ''.join([
              '--enable-xrc ',
              '--enable-intl ',
              '--enable-sound ',
              '--enable-timer ',
              '--enable-display ',
              '--enable-geometry ',
              '--enable-joystick ',
              '--enable-mediactrl ',
              '--enable-graphics_ctx ',
              '--disable-rpath ',
              '--disable-gtktest ',
              '--disable-sdltest ',
              '--disable-optimise ',
              '--disable-precomp-headers '
              '--with-sdl ',
              '--with-opengl ',
              '--with-zlib=sys ',
              '--with-expat=sys ',
              '--with-libpng=sys ',
              '--with-libxpm=sys ',
              '--with-libjpeg=sys ',
              '--with-libtiff=sys ',
              '--with-regex=builtin ',
              '--without-gnomevfs '
              ])
    shelltools.copytree("../wxWidgets-%s" % (get.srcVERSION().replace("_", "~")), "../wxWidgets-%s-gtk3" % get.srcVERSION())
    
    pisitools.flags.add("-fno-strict-aliasing")

    autotools.configure("--with-gtk=2 \
                         %s" % options)
    
    #shelltools.cd("%s" % get.workDIR())
    shelltools.cd("../wxWidgets-%s-gtk3" % get.srcVERSION())
    
    pisitools.flags.add("-fno-strict-aliasing")
    autotools.configure("--with-gtk=3 \
                         %s" % options)
    

def build():
    autotools.make()
    autotools.make("-C locale allmo")
    
    shelltools.cd("../wxWidgets-%s-gtk3" % get.srcVERSION())
    autotools.make()

def install():
    shelltools.cd("../wxWidgets-%s-gtk3" % get.srcVERSION())
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    pisitools.rename("/usr/bin/wx-config","wx-config-gtk3")
    
    shelltools.cd("../wxWidgets-%s" % get.srcVERSION())
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())
    #autotools.install()

    pisitools.dodoc("docs/licence*", "docs/*gpl*")
