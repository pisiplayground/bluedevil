#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

import os
from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools


def install():
    pisitools.dodir("/usr/bin")
    pisitools.dodir("/etc/profile.d")
    
    pisitools.insinto("/opt/maven", "bin")
    pisitools.insinto("/opt/maven", "boot")
    pisitools.insinto("/opt/maven", "conf")
    pisitools.insinto("/opt/maven", "lib")
    
    pisitools.dosym("/opt/maven/bin/mvn", "/usr/bin/mvn")
    pisitools.dosym("/opt/maven/bin/mvnDebug", "/usr/bin/mvnDebug")
    
    pisitools.dodoc("LICENSE", "README*")
