# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import cmaketools

def setup():
    # delete windows binaries
    shelltools.unlinkDir("bin")
    
    shelltools.makedirs("%s/jxrlib-0.2.4/java/target/swig/ome/jxrlib" % get.workDIR())
    shelltools.cd("java")
    shelltools.system("swig -java -c++ -package ome.jxrlib -outdir target/swig/ome/jxrlib -o target/swig/JXR_wrap.cxx JXR.i")
    
    
    options_cfg = ''.join([
                  '-DCMAKE_BUILD_TYPE=Release ',
                  '-DCMAKE_INSTALL_PREFIX=/usr '
                  ])
    shelltools.makedirs("../build")
    shelltools.cd("../build")
    cmaketools.configure("%s" % options_cfg, sourceDir="..")

def build():
    shelltools.cd("build")
    cmaketools.make()
    
    # build java library
    shelltools.cd("../java")
    shelltools.system("mvn install -DskipTests -Dmaven.repo.local='%s/jxrlib-0.2.4/repo'" % get.workDIR())

def install():
    shelltools.cd("build")
    cmaketools.rawInstall("DESTDIR=%s" % get.installDIR())
    
    shelltools.cd("../java/target")
    pisitools.insinto("/usr/share/java/%s-%s" % (get.srcNAME(), get.srcVERSION()), "%s-%s.jar" % (get.srcNAME(), get.srcVERSION()))
    
    shelltools.cd("../..")
    pisitools.dodoc("README*")